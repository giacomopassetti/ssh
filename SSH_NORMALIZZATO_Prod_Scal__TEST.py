# -*- coding: utf-8 -*-
"""
Created on Tue Oct  6 15:16:31 2020

@author: giaco
"""

   
import numpy as np
import numpy.linalg as alg
import scipy as sc
import scipy.linalg as alg2
import Scalar_DMRG as DMRG
import matplotlib.pyplot as plt
import math as math
import copy



    #Defines a string with the dimension of the whole Hilbert space
def IP(N,d):
    IP=(d,)
    i=1
    while i<N:
     IP+=(d,)
     i+=1;
    return IP;

 #Set system parameters and define the coeff. of the initial wave f. (For the moment works only if N=2n with n integer)
N=16
d=2
D=50
J=1
vv=1
t=0.1
T_tot=5
#C_Phi=np.random.rand(pow(d,N)).reshape(IP(N, d))
#Norm=alg.norm(C_Phi)
#Norm=1/Norm
#C_Phi=Norm*C_Phi
C_Phi=np.zeros(IP(N, d))
C_Phi[0,0,0,1,0,0,0,0,1,0,0,0,1,0,0,0]=1
P=C_Phi.reshape(d,pow(d,N-1))  
A_List=[]
P_0=[]
r_list=[]
r_list.append(1)



#While loop that implements the SVD decomposition of the MPS
i=0
while i<N-1:
    u,s,v=np.linalg.svd(P, full_matrices=False)
    S=np.diag(s)
    r=alg.matrix_rank(S)
    r_list.append(r)
    S=np.diag(s[0:r])
    V=np.zeros((r,pow(d,N-1-i)))
    V=v[0:r,:]
    P=S@V
    U=u[:,0:r].reshape(r_list[i],d,r)
    A_List.append(U)
    P_0.append(U)
    if i==N-2:
        P=P.reshape(r,d,1)
        A_List.append(P)
        P_0.append(P)
    P=P.reshape(d*r,pow(d,N-2-i))
    
    i+=1



print(DMRG.Scalar_MPS(P_0, P_0))
#Saving in another equal list the initial state for later calculations

#INSTRUCTIONS:
   #This while loop creates the liste A_List, where
   #A_List[i][j,k,l] has the meaning:
       #--i : site--,--j : a_j--,--k : sigma_i--, 
       #--l : a_l
   
             #MPO



# Single site Number of particles operator

    
    
print(DMRG.N_i(P_0, 1))
    

# Set of system parameter and definition of odd Bond H coeff. and H_e even coef.
  #(Watch out that is defined for d=2)

H=np.zeros((d,d,d,d))
H[1,0,0,1]=J
H[0,1,1,0]=J
H_e=np.zeros((d,d,d,d))
H_e[1,0,0,1]=vv
H_e[0,1,1,0]=vv


#Creation of unitary evol. coeff. and index ordering.
         #ODD
O=alg2.expm(-1j*t*H.reshape(4,4))
O=O.reshape(2,2,2,2)
O=np.transpose(O,(0,2,1,3))
O=O.reshape(4,4)
         #EVEN
O_e=alg2.expm(-1j*t*H_e.reshape(4,4))
O_e=O_e.reshape(2,2,2,2)
O_e=np.transpose(O_e,(0,2,1,3))
O_e=O_e.reshape(4,4)


#SVD decomposition of the time ev. op. in u and v
u,s,v=alg.svd(O, full_matrices=True)
r_s=np.sqrt(s)
r_S=np.diag(r_s)
r=alg.matrix_rank(r_S)
u=u[:,0:r]
v=v[0:r,:]
U=np.zeros((4,r),dtype=complex)
V=np.zeros((r,4),dtype=complex)
for i in range(4):
    for j in range(r):
        U[i,j]=u[i,j]*r_s[j]
for i in range(r):
    for j in range(4):
        V[i,j]=v[i,j]*r_s[i]        
U=U.reshape(2,2,1,r)
V=V.reshape(r,1,2,2) 
V=np.transpose(V,(2,3,0,1))  
# Here U[i,j,k,l] has : --i : s_1-- 
#--j : s'_1-- -- k dummy -- l : "k"--
# Here V[i,j,k,l] has : --i : s_2-- 
#--j : s'_2-- -- k dummy "k"-- l : dummy--

u_e,s_e,v_e=alg.svd(O_e, full_matrices=True)
r_s_e=np.sqrt(s_e)
r_S_e=np.diag(r_s_e)
r_e=alg.matrix_rank(r_S_e)
u_e=u_e[:,0:r_e]
v_e=v_e[0:r_e,:]
U_e=np.zeros((4,r_e),dtype=complex)
V_e=np.zeros((r_e,4),dtype=complex)
for i in range(4):
    for j in range(r_e):
        U_e[i,j]=u_e[i,j]*r_s_e[j]
for i in range(r_e):
    for j in range(4):
        V_e[i,j]=v_e[i,j]*r_s_e[i]        
U_e=U_e.reshape(2,2,1,r)
V_e=V_e.reshape(r,1,2,2) 
V_e=np.transpose(V_e,(2,3,0,1))  
# Here U[i,j,k,l] has : --i : s_1-- 
#--j : s'_1-- -- k dummy -- l : "k"--
# Here V[i,j,k,l] has : --i : s_2-- 
#--j : s'_2-- -- k dummy "k"-- l : dummy--

      #  TIME EVOLUTION
          #Odd bonds

temp_step=0
times=[]
Pt=[]
while temp_step<T_tot:
 
 No_list=[]
 for i in range(int(N/2)):
    NN=np.zeros(shape=(d,U.shape[2],A_List[2*i].shape[0],U.shape[3],A_List[2*i].shape[2]),dtype=complex)
    for j in range(d):
        for k in range(U.shape[2]):
            for l in range(A_List[2*i].shape[0]):
                for m in range(U.shape[3]):
                    for n in range(A_List[2*i].shape[2]):
                        NN[j,k,l,m,n]=U[j,0,k,m]*A_List[2*i][l,0,n]+U[j,1,k,m]*A_List[2*i][l,1,n]
    No_list.append(NN)
 Ne_list=[]
 for i in range(int(N/2)):
    NN=np.zeros(shape=(d,V.shape[2],A_List[2*i+1].shape[0],V.shape[3],A_List[2*i+1].shape[2]),dtype=complex)
    for j in range(d):
        for k in range(V.shape[2]):
            for l in range(A_List[2*i+1].shape[0]):
                for m in range(V.shape[3]):
                    for n in range(A_List[2*i+1].shape[2]):
                        NN[j,k,l,m,n]=V[j,0,k,m]*A_List[2*i+1][l,0,n]+V[j,1,k,m]*A_List[2*i+1][l,1,n]
    Ne_list.append(NN)                    


 N_list=[]
 for i in range(int(N/2)):
    N_list.append(No_list[i].reshape(d,U.shape[2]*A_List[2*i].shape[0],U.shape[3]*A_List[2*i].shape[2]).transpose(1,0,2))
    N_list.append(Ne_list[i].reshape(d,V.shape[2]*A_List[2*i+1].shape[0],V.shape[3]*A_List[2*i+1].shape[2]).transpose(1,0,2))
                         
#The Element of N_List are 3-d arrays which arguments are defined by:
             #N[i,j,k] with -- i: a1-- -- j: sigma-- -- k: "a2"--   
                 
             
                
                       #IMPLEMENTATION OF THE EVEN BONDS
 Noe_list=[]
 for i in range(int(N/2)-1):
    NN=np.zeros(shape=(d,U_e.shape[2],N_list[2*i+1].shape[0],U_e.shape[3],N_list[2*i+1].shape[2]),dtype=complex)
    for j in range(d):
        for k in range(U_e.shape[2]):
            for l in range(N_list[2*i+1].shape[0]):
                for m in range(U_e.shape[3]):
                    for n in range(N_list[2*i+1].shape[2]):
                        NN[j,k,l,m,n]=U_e[j,0,k,m]*N_list[2*i+1][l,0,n]+U_e[j,1,k,m]*N_list[2*i+1][l,1,n]
    Noe_list.append(NN)
 Nee_list=[]
 for i in range(int(N/2)-1):
    NN=np.zeros(shape=(d,V_e.shape[2],N_list[2*i+2].shape[0],V_e.shape[3],N_list[2*i+2].shape[2]),dtype=complex)
    for j in range(d):
        for k in range(V_e.shape[2]):
            for l in range(N_list[2*i+2].shape[0]):
                for m in range(V_e.shape[3]):
                    for n in range(N_list[2*i+2].shape[2]):
                        NN[j,k,l,m,n]=V_e[j,0,k,m]*N_list[2*i+2][l,0,n]+V_e[j,1,k,m]*N_list[2*i+2][l,1,n]
    Nee_list.append(NN)   


#This for loop overwrite the "bulk" coefficients and generate the list of the state obtained after a temporal evolution t.
 for i in range(int(N/2)-1):
    N_list[2*i+1]=Noe_list[i].reshape(d,U_e.shape[2]*N_list[2*i+1].shape[0],U_e.shape[3]*N_list[2*i+1].shape[2]).transpose(1,0,2)
    N_list[2*i+2]=Nee_list[i].reshape(d,V_e.shape[2]*N_list[2*i+2].shape[0],V_e.shape[3]*N_list[2*i+2].shape[2]).transpose(1,0,2)


# At this point N_List contains the coefficients after an infinitesimal time evolution.


 

                     # COMPRESSION
                     #--- Part I : Left Canonical normalization
 for i in range(N-1):
      u,s,v=alg.svd(N_list[i].reshape(N_list[i].shape[0]*N_list[i].shape[1],N_list[i].shape[2]), full_matrices=True)
      S=np.diag(s)
      r=alg.matrix_rank(S)
      r_list[i+1]=r
      
      Vtt=np.zeros((r,N_list[i].shape[2]), dtype=complex)
      for j in range(r):
         Vtt[j,:]=s[j]*v[j,:]
      N_list[i+1]=np.tensordot(Vtt, N_list[i+1], axes=(1,0))
      A_List[i]=u[:,0:r].reshape(r_list[i],d,r)
      
                    
         
 A_List[N-1]=N_list[N-1]
 rr_list=[A_List[N-1].shape[2]]                     #Then starting from the last site right canonical with compression to a maximum D dimension
 for i in range(N-1):
     u,s,v=alg.svd(A_List[N-i-1].reshape(A_List[N-i-1].shape[0],A_List[N-i-1].shape[1]*A_List[N-i-1].shape[2]), full_matrices=True)
     S=np.diag(s)
     r=alg.matrix_rank(S)
     rr_list.append(r)
     
     if r>D:
         
         s=s[0:D]
         u=u[:,0:D]
         v=v[0:D,:]
         Norm=alg.norm(s)
         Norm=1/Norm
         s=Norm*s
         Utt=np.zeros((A_List[N-i-1].shape[0],D), dtype=complex)
         for j in range(D):
             
             Utt[:,j]=s[j]*u[:,j]
         
         A_List[N-i-2]=np.tensordot(A_List[N-i-2], Utt, axes=(2,0))
         A_List[N-i-1]=v.reshape(D,d,int(v.shape[1]/d)) 
         
     else:
         s=s[0:r]
         u=u[:,0:r]
         v=v[0:r,:]
         Utt=np.zeros((A_List[N-i-1].shape[0],r), dtype=complex)
         for j in range(r):
           Utt[:,j]=s[j]*u[:,j]
         
         A_List[N-i-2]=np.tensordot(A_List[N-i-2], Utt, axes=(2,0))
         A_List[N-i-1]=v.reshape(r,d,int(v.shape[1]/d))        
 
    #For loop that performs the scalar product <Pt|P_0>
 Pt.append(float(abs(DMRG.Scalar_MPS(A_List, P_0))))
 print(DMRG.Scalar_MPS(A_List, P_0))
 
 
 times.append(temp_step)
 temp_step+=t


Nii=[]
for i in range(N):
    print("AAAAAAA:  ", float(abs(DMRG.N_i(A_List, i))))
    Nii.append(float(abs(DMRG.N_i(A_List, i))))
           
print(DMRG.N_i(P_0, 1))
plt.plot(times, Pt)
plt.show()

plt.plot(Nii)
plt.show()


  
    
    
      
      
                       


    
    